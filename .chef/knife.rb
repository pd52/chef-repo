# See http://docs.chef.io/config_rb_knife.html for more information on knife configuration options

current_dir = File.dirname(__FILE__)
log_level                :info
log_location             STDOUT
node_name                "pd52"
client_key               "#{current_dir}/pd52.pem"
chef_server_url          "https://api.chef.io/organizations/arc_training_pd52"
cookbook_path            ["#{current_dir}/../cookbooks"]
